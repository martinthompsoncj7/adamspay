<?php 
  require 'config/config.php';
  require 'config/conexion_db.php';
  $db = new Database();
  $con = $db->conectar();

  $id = isset($_GET['id']) ? $_GET['id'] : '';
  $token = isset($_GET['token']) ? $_GET['token'] : '';

if ($id == '' || $token == '') {
  echo 'Error al procesar la peticion';
  exit; 
}else{

  $token_tmp = hash_hmac('sha1', $id, KEY_TOKEN);

  if ($token == $token_tmp) {

    $sql = $con->prepare("SELECT count(id) FROM productos WHERE id=? AND activo=1");
    $sql->execute([$id]);
    if ($sql->fetchColumn() > 0) {

      $sql = $con->prepare("SELECT nombre, descripcion, precio, descuento  FROM productos WHERE id=? AND activo=1 LIMIT 1");
      $sql->execute([$id]);
      $row = $sql->fetch(PDO::FETCH_ASSOC);
      $nombre = $row['nombre'];
      $descripcion = $row['descripcion'];
      $precio = $row['precio'];
      $descuento = $row['descuento'];
      $precio_descuento = $precio - (($precio * $descuento) / 100);
      $dir_images = 'images/productos/' . $id . '/';
      $rutaImg = $dir_images . 'principal.jpg';

      if (!file_exists($rutaImg)) {
        $rutaImg = 'images/nofoto.png';
      }
      $imagenes = array();
      if (file_exists($dir_images)) {
        $dir = dir($dir_images);

        while (($archivo = $dir->read()) != false) {
          if ($archivo != 'principal.jpg' && (strpos($archivo, 'jpg') || strpos($archivo, 'jpeg'))) {
            $imagenes[] = $dir_images . $archivo;
          }
        }
        $dir->close();
      }
    }

  } else {
    echo 'Error al procesar la peticion';
  exit; 
  }

}

 
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.104.2">
    <title>AdamsPay</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/album/">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="css/estilo.css">
    <link rel="stylesheet" href="css/blog.css">
    <link rel="icon" href="images/Adamspay/logo.svg">

    <script src="https://kit.fontawesome.com/41bcea2ae3.js" crossorigin="anonymous"></script>

    

    

<link href="/docs/5.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">

    <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/5.2/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/5.2/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/5.2/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/5.2/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/5.2/assets/img/favicons/safari-pinned-tab.svg" color="#712cf9">
<link rel="icon" href="/docs/5.2/assets/img/favicons/favicon.ico">
<meta name="theme-color" content="#712cf9">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      .b-example-divider {
        height: 3rem;
        background-color: rgba(0, 0, 0, .1);
        border: solid rgba(0, 0, 0, .15);
        border-width: 1px 0;
        box-shadow: inset 0 .5em 1.5em rgba(0, 0, 0, .1), inset 0 .125em .5em rgba(0, 0, 0, .15);
      }

      .b-example-vr {
        flex-shrink: 0;
        width: 1.5rem;
        height: 100vh;
      }

      .bi {
        vertical-align: -.125em;
        fill: currentColor;
      }

      .nav-scroller {
        position: relative;
        z-index: 2;
        height: 2.75rem;
        overflow-y: hidden;
      }

      .nav-scroller .nav {
        display: flex;
        flex-wrap: nowrap;
        padding-bottom: 1rem;
        margin-top: -1px;
        overflow-x: auto;
        text-align: center;
        white-space: nowrap;
        -webkit-overflow-scrolling: touch;
      }
    </style>

    
  </head>
  <body>
    
    <a href="https://web.whatsapp.com/" class="btn-wsp" target="_blank">
      <i class="fa-brands fa-whatsapp"></i>
    </a>
    <a href="https://ar-ar.facebook.com/people/AdamsPay/100064134215400/?ref=page_internal" class="btn-fk" target="_blank">
      <i class="fa-brands fa-facebook-f"></i>
    </a>
    <a href="https://www.instagram.com/adamspaypy/" class="btn-ig" target="_blank">
      <i class="fa-brands fa-instagram"></i>
    </a>

  <header>
    <div class="navbar navbar-expand-lg navbar-dark" style="background: #D35400; opacity: 80%;">
      <div class="container">
        <a href="#" class="navbar-brand">
        <a href="#"><img src="images/Adamspay/logo.svg" width="70px" height="70px" style="border-radius: 37px; opacity: 80%;" alt=""></a>
          <div class="cabecera">
                <ul id="anima">
                  <li><Strong style="font-family: 'Emilys Candy', cursive; font-size: 25px;">Adamspay</Strong></li><br><br>
                  <li><strong style="font-family: 'Emilys Candy', cursive; font-size: 25px;">¡Bienvenidos!</strong></li><br><br>
                </ul>
          </div>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarHeader">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a href="#" class="nav-link active" style="font-size: 20px;">Detalles</a>
                </li>

                <li class="nav-item">
                    <a id="hover" href="blog.php" class="nav-link" style="font-size: 20px;">Productos</a>
                </li>
            </ul>
            <a href="checkout.php" id="carrito"><img src="images/shopping2.ico" width="50" height="50" alt=""><span id="num_cart" class="badge" style="background: white; color: black; font-size: 14px; opacity: 100%; border-radius: 90%;"><strong><?php echo $num_cart;?></strong></span>
        </a>
        </div>
      </div>
    </div>
  </header>

  <main>
    <div class="container">
        <div class="row">
          <div class="col-md-6 order-md-1">

            <div id="carouselImages" class="carousel slide" data-bs-ride="carousel">
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img id="prod" src="<?php echo $rutaImg;?>" class="d-block w-100">
                </div>
                <?php foreach($imagenes as $img){?>
                <div class="carousel-item">
                  <img id="prod" src="<?php echo $img;?>" class="d-block w-100">
                </div>
                <?php } ?>
              </div>
              <button class="carousel-control-prev" type="button" data-bs-target="#carouselImages" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
              </button>
              <button class="carousel-control-next" type="button" data-bs-target="#carouselImages" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
              </button>
            </div>

            
          </div>
          <div class="col-md-6 order-md-2">
            <h2><?php echo $nombre; ?></h2>
            <?php if ($descuento > 0) { ?>
              <p><del><?php echo MONEDA . number_format($precio, 2, '.', ','); ?></del></p>
              <h2>
              <?php echo MONEDA . number_format($precio_descuento, 2, '.', ','); ?>
              <small class="text-success"><?php echo $descuento ?>% descuento</small>
              </h2>

              <?php } else { ?>
                
            <h2><?php echo MONEDA . number_format($precio, 2, '.', ','); ?></h2>
            <?php }?>
            <p class="lead">
              <?php echo $descripcion; ?>
            </p>
            <div class="g-grid gap-3 col-10 max-auto">
              <button id="comprar" class="btn btn-primary" type="button">Comprar Ahora</button>
              <button id="agregarc" class="btn btn-outline-primary" type="button" onclick="addProducto(<?php echo $id; ?>, '<?php echo $token_tmp?>')">Agregar al carrito</button>
            </div>
          </div>
        </div>
    </div>

  </main>

  <div class="container">
      <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
        <div class="col-md-4 d-flex align-items-center">
          <a href="/" class="mb-3 me-2 mb-md-0 text-muted text-decoration-none lh-1">
            <svg class="bi" width="30" height="24"><use xlink:href="#bootstrap"/></svg>
          </a>
          <img src="images/Adamspay/logo.svg" width="40px" height="40px" style="border-radius: 20px;"><span class="mb-3 mb-md-0 text-muted">&copy; 2022 AdamsPay</span>
        </div>

        <ul class="nav col-md-4 justify-content-end list-unstyled d-flex">
          <li class="ms-3"><a class="text-muted" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#twitter"/></svg></a></li>
          <li class="ms-3"><a class="text-muted" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#instagram"/></svg></a></li>
          <li class="ms-3"><a class="text-muted" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#facebook"/></svg></a></li>
        </ul>
      </footer>
    </div>

    <div class="container">
      <footer class="py-3 my-4">
        <ul class="nav justify-content-center border-bottom pb-3 mb-3">
          <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Quienes somos</a></li>
          <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Metodos de pago</a></li>
          <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Preguntas frecuentes</a></li>
          <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Promociones</a></li>
        </ul>
        <p class="text-center text-muted">&copy; 2022 Martin Thompson Centurion</p>
      </footer>
    </div>


    <script src="/docs/5.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    <!--SCRIPTS-->
    <script src="controlador/jquery-3.6.1.min.js"></script>
    <script src="controlador/script.js"></script>
    <script src="controlador/carrito-controlador.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>  
    <script>
      function addProducto(id, token){
        let url = 'clases/carrito.php'
        let formData = new FormData()
        formData.append('id', id)
        formData.append('token', token)

        fetch (url, {
          method: 'POST',
          body: formData,
          mode: 'cors'
        }).then(response => response.json())
        .then(data =>{
          if(data.ok){
            let elemento = document.getElementById("num_cart")
            elemento.innerHTML = data.numero
          }
        })
      }
    </script> 
    

  </body>
</html>
