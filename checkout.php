<?php 
  require 'config/config.php';

  require 'config/conexion_db.php';
  $db = new Database();
  $con = $db->conectar();

  $productos = isset($_SESSION['carrito']['productos']) ? $_SESSION['carrito']['productos'] : null;

    $lista_carrito = array();

if ($productos != null) {

    foreach ($productos as $clave => $cantidad){

  $sql = $con->prepare("SELECT id, nombre, precio, descuento, $cantidad AS cantidad FROM productos WHERE id=? AND activo=1");
  $sql->execute([$clave]);
  $lista_carrito[] = $sql->fetch(PDO::FETCH_ASSOC);
  /* session_destroy(); */
  /* print_r($_SESSION); */
    }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title id="titulo">AdamsPay</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Anton&family=Montserrat+Subrayada&family=Montserrat:wght@400;700&family=Roboto&display=swap" rel="stylesheet">

    <script src="https://kit.fontawesome.com/41bcea2ae3.js" crossorigin="anonymous"></script>
    <script src="https://www.paypal.com/sdk/js?client-id=ARQmxdwPDs_XH4e4YSrJemvWBGASSbCtQiVBud7xee6M4rKSPkVqP62w7_bXHjqCWjOrWAxUsYwCIraS&currency=USD"></script>

    

    <link rel="icon" href="image/navidad.png">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <!--BOOTSTRAP-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="css/articulos.css">
    <link rel="stylesheet" href="css/principal.css">
    <link rel="stylesheet" href="css/blog.css">
    <link rel="icon" href="images/Adamspay/logo.svg">

</head>
<body>

    <a href="https://web.whatsapp.com/" class="btn-wsp" target="_blank">
      <i class="fa-brands fa-whatsapp"></i>
    </a>
    <a href="https://ar-ar.facebook.com/people/AdamsPay/100064134215400/?ref=page_internal" class="btn-fk" target="_blank">
      <i class="fa-brands fa-facebook-f"></i>
    </a>
    <a href="https://www.instagram.com/adamspaypy/" class="btn-ig" target="_blank">
      <i class="fa-brands fa-instagram"></i>
    </a>

    <!--Header - menu-->
  <header>
    <div class="navbar navbar-expand-lg navbar-dark " style="background: #D35400; opacity: 80%;">
      <div class="container">
        <a href="#" class="navbar-brand">
        <a href="#"><img src="images/Adamspay/logo.svg" width="70px" height="70px" style="border-radius: 37px; opacity: 80%;" alt=""></a>
          <div class="cabecera">
                <ul id="anima">
                  <li><Strong style="font-family: 'Emilys Candy', cursive; font-size: 25px;">AdamsPay</Strong></li><br><br>
                  <li><strong style="font-family: 'Emilys Candy', cursive; font-size: 25px;">¡Bienvenidos!</strong></li><br><br>
                </ul>
          </div>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarHeader">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a href="#" class="nav-link active" style="font-size: 20px;">Carrito</a>
                </li>
                <li class="nav-item">
                    <a href="index.php" class="nav-link" style="font-size: 20px;">Productos</a>
                </li>

                <li class="nav-item">
                    <a id="hover" href="https://www.google.com/search?q=whatsapp+web&oq=wha&aqs=chrome.1.69i57j69i59j69i60.3657j0j1&sourceid=chrome&ie=UTF-8" target="_blank" class="nav-link" style="font-size: 20px;">Contacto</a>
                </li>
            </ul>
            <a href="checkout.php" id="carrito"><img src="images/shopping2.ico" width="50" height="50" alt=""><span id="num_cart" class="badge" style="background: blue; color: white; font-size: 14px; opacity: 100%; border-radius: 90%; font-size: 14px;"><strong><?php echo $num_cart;?></strong></span>
        </a>
        </div>
      </div>
    </div>
  </header>
    <!-- <div class="container-post">

        <input type="radio" id="TODOS" name="categories" value="TODOS" checked>
        <input type="radio" id="Matemáticas" name="categories" value="Matemáticas">
        <input type="radio" id="Historia" name="categories" value="CSS">
        <input type="radio" id="Fisica" name="categories" value="JS">
        <input type="radio" id="Idiomas" name="categories" value="PHP">

        <div class="container-category">
            <label class="label" for="TODOS">TODOS</label>
            <label class="label" for="HTML">Pasteles</label>
            <label class="label" for="CSS">agregar</label>
            <label class="label" for="JS">agregar</label>
            <label class="label" for="PHP">agregar</label>

        </div> -->

    <!--Portada-->

    <div class="container-all" id="move-content">

  
    
    <main>
        <div class="album py-5 bg-light" style="margin-top: 55px;">
        <div class="container">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Productos</th>
                            <th>Precio</th>
                            <th>Cantidad</th>
                            <th>Subtotal</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($lista_carrito == null) {
                            echo '<tr><td colspan="5" class="text-center"><b>Lista vacía</b></td></tr>';
                        } else {
                            $total = 0;
                            foreach($lista_carrito as $producto){
                                $_id = $producto['id'];
                                $nombre = $producto['nombre'];
                                $precio = $producto['precio'];
                                $descuento = $producto['descuento'];
                                $cantidad = $producto['cantidad'];
                                $precio_descuento = $precio - (($precio * $descuento)/100);
                                $subtotal = $cantidad * $precio_descuento;
                                $total += $subtotal;
                            ?>
                        <tr>
                            <td><?php echo $nombre;?></td>
                            <td><?php echo MONEDA . number_format($precio_descuento, 2, '.',',');?></td>
                            <td>
                                
                                <input type="number" min="1" max="10" step="1" value="<?php echo $cantidad?>"
                                size="5" id="cantidad_<?php echo $_id; ?>" onchange="actualizaCantidad(this.value, <?php echo $_id;?>)">
                            </td>
                            <td>
                                <div id="subtotal_<?php echo $_id?>" name="subtotal[]"><?php echo MONEDA . number_format($subtotal, 2, '.',',');?></div>
                            </td>
                            <td><a id="eliminar" class="btn btn-warning btn-sm" data-bs-id="<?php echo $_id;?>" data-bs-toggle="modal" data-bs-target="#eliminaModal">Eliminar</a></td>
                        </tr>
                        <?php }?>

                                <tr>
                                    <td colspan="3"></td>
                                    <td colspan="2">
                                        <p class="h3" id="total"><?php echo MONEDA . number_format($total, 2, '.', ',');?></p>
                                    </td>
                                </tr>
                    </tbody>
                    <?php }?>
                </table>
            </div>
            <?php if ($lista_carrito != null) { ?>
            <div class="row">
                <div class="col-md-5 offset-md-7 d-grid gap-2">
                    <a id="btn-pago" href="pago.php" class="btn btn-primary btn-lg" style="background: #F1C40F; color: #34495E;"><strong>Realizar pago</strong></a>
                </div>
            </div>
            <?php } ?>
        </div>
        </div>

    </main>
        <!-- Modal -->
        <div class="modal fade" id="eliminaModal" tabindex="-1" aria-labelledby="eliminaModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="eliminaModalLabel">Alerta</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            ¿Desea Eliminar el producto de la lista?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                <button id="btn-elimina" type="button" class="btn btn-danger" onclick="eliminar()">Eliminar</button>
            </div>
            </div>
        </div>
        </div>

    </div>
    


    <div class="b-example-divider"></div>

    <div class="container">
  <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
    <div class="col-md-4 d-flex align-items-center">
      <a href="/" class="mb-3 me-2 mb-md-0 text-muted text-decoration-none lh-1">
        <svg class="bi" width="30" height="24"><use xlink:href="#bootstrap"/></svg>
      </a>
      <img src="images/Adamspay/logo.svg" width="40px" height="40px" style="border-radius: 20px;"><span class="mb-3 mb-md-0 text-muted">&copy; 2022 AdamsPay</span>
    </div>

    <ul class="nav col-md-4 justify-content-end list-unstyled d-flex">
      <li class="ms-3"><a class="text-muted" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#twitter"/></svg></a></li>
      <li class="ms-3"><a class="text-muted" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#instagram"/></svg></a></li>
      <li class="ms-3"><a class="text-muted" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#facebook"/></svg></a></li>
    </ul>
  </footer>
</div>

<div class="b-example-divider"></div>

<div class="container">
  <footer class="py-3 my-4">
    <ul class="nav justify-content-center border-bottom pb-3 mb-3">
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Quienes somos</a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Metodos de pago</a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Preguntas frecuentes</a></li>
      <li class="nav-item"><a href="#" class="nav-link px-2 text-muted">Promociones</a></li>
    </ul>
    <p class="text-center text-muted">&copy; 2022 Martin Thompson Centurion</p>
  </footer>
</div>

    </div>
    <!--SCRIPTS-->
    <script src="controlador/jquery-3.6.1.min.js"></script>
    <script src="controlador/script.js"></script>
    <script src="controlador/carrito-controlador.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
    <script>

        let eliminaModal = document.getElementById('eliminaModal')
            eliminaModal.addEventListener('show.bs.modal', function(event){
                let button = event.relatedTarget
                let id = button.getAttribute('data-bs-id')
                let buttonElimina = eliminaModal.querySelector('.modal-footer #btn-elimina')
                buttonElimina.value = id
            })

      function actualizaCantidad(cantidad, id){
        let url = 'clases/actualizar_carrito.php'
        let formData = new FormData()
        formData.append('action', 'agregar')
        formData.append('id', id)
        formData.append('cantidad', cantidad)

        fetch (url, {
          method: 'POST',
          body: formData,
          mode: 'cors'
        }).then(response => response.json())
        .then(data =>{
          if(data.ok){

            let divsubtotal = document.getElementById('subtotal_' + id)
            divsubtotal.innerHTML = data.sub

            let total = 0.00
            let list = document.getElementsByName('subtotal[]')

            for (let i = 0; i < list.length; i++) {
                total += parseFloat(list[i].innerHTML.replace(/[$,]/g, ''))  
            }
            total = new Intl.NumberFormat('en-US',{
                minimumFractionDigits: 2
            }).format(total)
            document.getElementById('total').innerHTML = '<?php echo MONEDA;?>' + total
          }
        })
      }

      function eliminar(){

        let botonElimina = document.getElementById('btn-elimina')
        let id = botonElimina.value

        let url = 'clases/actualizar_carrito.php'
        let formData = new FormData()
        formData.append('action', 'eliminar')
        formData.append('id', id)
        
        fetch (url, {
          method: 'POST',
          body: formData,
          mode: 'cors'
        }).then(response => response.json())
        .then(data =>{
          if(data.ok){
            location.reload()
          }
        })
      } 
    </script> 
    
  </body>
</html>
